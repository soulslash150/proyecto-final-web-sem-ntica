Autores:	Agustín Mellado B.
			Eduardo González B.

Para ejecutar el endpoint SPARQL (Fuseki):
	cd apache-jena-fuseki-2.3.1
	java -jar fuseki-sver.jar --loc=../drugbank2 /db

Para acceder al endpoint ya cargado se debe ingresar a http://localhost:3030

Para ejecutar la página web (Node):
	cd PaginaWebNode
	npm start

Para acceder al endpoint ya cargado se debe ingresar a http://localhost:3000	