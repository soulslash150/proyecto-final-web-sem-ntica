var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/avanzado',function(req,res,next){
	res.render('avanzado');
});

router.get('/consultasEstaticas',function(req,res,next){
	res.render('consultasEstaticas');
});

router.get('/consultasDinamicas',function(req,res,next){
	res.render('consultasDinamicas');
});

router.get('/holamundo',function(req,res){
	res.render('holamundo',{title: "Hola Mundo!"});
});

router.get('/usuarios',function(req,res){
	var db = req.db;
	var collection = db.get('usercollection');
	collection.find({},{},function(e,docs){
		res.render('userlist',{
			"userlist" : docs
		});
	});
});
module.exports = router;
