$(document).ready(function(){
	$("#menu-dinamicas").addClass("active");
	$(".datepicker").datepicker();
	$(".datepicker").datepicker("option","dateFormat","yy-mm-dd");

	$("#consulta1-ejecutar").click(function(){
		$("#consulta1-ejecutar").prop("disabled", true);
		$("#consulta1-f1").prop("disabled",true);
		$("#consulta1-f2").prop("disabled",true);
		$("#consulta1-body-tabla").empty();
		var query="PREFIX lsqv: <http://bio2rdf.org/logs_vocabulary:> PREFIX bio2rdf: <http://bio2rdf.org/> PREFIX purl: <http://purl.org/dc/terms/> PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> SELECT ?C ?D ?F WHERE { ?A purl:issued ?B . ?C lsqv:execution ?A . ?C ?D ?F FILTER (?D != lsqv:execution && ?B > '"+$("#consulta1-f1").val()+"'^^xsd:date && ?B < '"+$("#consulta1-f2").val()+"'^^xsd:date)} ORDER BY ASC (?C)";
		$.ajax({
			type: "POST",
			url: "http://localhost:3030/db/query",
			data: {query: query},
			success: function(data){
				$("#consulta1-ejecutar").prop("disabled", false);
				$("#consulta1-f1").prop("disabled",false);
				$("#consulta1-f2").prop("disabled",false);
				var resultados = data.results.bindings;
				for(var i = 0; i < resultados.length; i++){
					$("#consulta1-body-tabla").append("<tr><td>"+resultados[i].C.value+"</td><td>"+resultados[i].D.value+"</td><td>"+resultados[i].F.value+"</td></tr>");
				}
			},
			error: function(x,a,s){
				alert("Se ha producido un error...");
				$("#consulta1-ejecutar").prop("disabled", false);
				$("#consulta1-f1").prop("disabled",false);
				$("#consulta1-f2").prop("disabled",false);
			}
		});
	});

	$("#botonBuscarPorID").click(function(){
		if($("#consulta2-input").val() != ""){
			$("#consulta2-body-tabla").empty();
			$("#consulta2-input").prop("disabled",true);
			$("#consulta2-boton-selector").prop("disabled",true);
			$.ajax({
				type: "POST",
				url: "http://bio2rdf.org/sparql?format=application%2Fsparql-results%2Bjson",
				data: {query: "PREFIX purl: <http://purl.org/dc/terms/> PREFIX yay: <http://bio2rdf.org/drugbank:> SELECT * WHERE{ yay:"+$("#consulta2-input").val()+" purl:title ?nombre . yay:"+$("#consulta2-input").val()+" purl:description ?descripcion}"},
				success: function(data){
					$("#consulta2-input").prop("disabled",false);
					$("#consulta2-boton-selector").prop("disabled",false);
					var datos = data.results.bindings;
					if(datos.length != 0){
						$("#consulta2-body-tabla").append("<tr><td>"+$("#consulta2-input").val()+"</td><td>"+datos[0].nombre.value+"</td><td>"+datos[0].descripcion.value+"</td></tr>");
					}else{
						alert("No se encontraron resultados...");
					}
				},
				error: function(a,b,c){
					$("#consulta2-input").prop("disabled",false);
					$("#consulta2-boton-selector").prop("disabled",false);
					alert("Se ha producido un error...");
				}
			});
		}else{
			alert("Ingrese un parámetro de búsqueda");
		}
	});

	$("#botonBuscarPorNombre").click(function(){
		if($("#consulta2-input").val() != ""){
			$("#consulta2-body-tabla").empty();
			$("#consulta2-input").prop("disabled",true);
			$("#consulta2-boton-selector").prop("disabled",true);
			$.ajax({
				type: "POST",
				url: "http://bio2rdf.org/sparql?format=application%2Fsparql-results%2Bjson",
				data: {query: "PREFIX purl: <http://purl.org/dc/terms/> PREFIX yay: <http://bio2rdf.org/drugbank:> SELECT * WHERE{ ?ID purl:title '"+$("#consulta2-input").val()+"'@en . ?ID purl:description ?descripcion FILTER regex(str(?ID),'DB')}"},
				success: function(data){
					$("#consulta2-input").prop("disabled",false);
					$("#consulta2-boton-selector").prop("disabled",false);
					var datos = data.results.bindings;
					if(datos.length != 0){
						$("#consulta2-body-tabla").append("<tr><td>"+datos[0].ID.value.split("k:")[1]+"</td><td>"+$("#consulta2-input").val()+"</td><td>"+datos[0].descripcion.value+"</td></tr>");
					}else{
						alert("No se encontraron resultados...");
					}
				},
				error: function(a,b,c){
					$("#consulta2-input").prop("disabled",false);
					$("#consulta2-boton-selector").prop("disabled",false);
					alert("Se ha producido un error...");
				}
			});
		}else{
			alert("Ingrese un parámetro de búsqueda");
		}
	});


	$("#consulta3-ejecutar").click(function(){
		if($("#consulta3-input").val() != ""){
			$("#consulta3-ejecutar").prop("disabled", true);
			$("#consulta3-input").prop("disabled",true);
			$("#consulta3-body").empty();
			var query="PREFIX lsqv: <http://bio2rdf.org/logs_vocabulary:> PREFIX bio2rdf: <http://bio2rdf.org/> PREFIX purl: <http://purl.org/dc/terms/> SELECT ?C (COUNT(?C) as ?SUMA) WHERE { ?A lsqv:hasUri bio2rdf:drugbank:"+$("#consulta3-input").val()+" . ?A lsqv:execution ?B . ?B purl:issued ?C } GROUP BY (?C) ORDER BY ASC (?C)";
			$.ajax({
				type: "POST",
				url: "http://localhost:3030/db/query",
				data: {query: query},
				success: function(data){
					$("#consulta3-ejecutar").prop("disabled", false);
					$("#consulta3-input").prop("disabled",false);
					var datos = data.results.bindings;

					var margin = {top: 20, right: 20, bottom: 30, left: 50},
					    width = 800 - margin.left - margin.right,
					    height = 500 - margin.top - margin.bottom;

					var formatDate = d3.time.format("%Y-%m-%d");

					var x = d3.time.scale()
					    .range([0, width]);

					var y = d3.scale.linear()
					    .range([height, 0]);

					var xAxis = d3.svg.axis()
					    .scale(x)
					    .orient("bottom");

					var yAxis = d3.svg.axis()
					    .scale(y)
					    .orient("left");

					var line = d3.svg.line()
					    .x(function(d) { return x(d.date); })
					    .y(function(d) { return y(d.close); });

					var svg = d3.select("#consulta3-body").append("svg")
					    .attr("width", width + margin.left + margin.right)
					    .attr("height", height + margin.top + margin.bottom)
					  .append("g")
					    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

					var datax = [];

					for(var i = 0; i < datos.length; i++){
						var aux = datos[i];
						datax.push({date: new Date(aux.C.value),close: parseInt(aux.SUMA.value)});
					}

					data = datax;
					x.domain(d3.extent(data, function(d) { return d.date; }));
					y.domain(d3.extent(data, function(d) { return d.close; }));

					svg.append("g")
					    .attr("class", "x axis")
					    .attr("transform", "translate(0," + height + ")")
					    .call(xAxis);

					svg.append("g")
					    .attr("class", "y axis")
					    .call(yAxis)
					  .append("text")
					    .attr("transform", "rotate(-90)")
					    .attr("y", 6)
					    .attr("dy", ".71em")
					    .style("text-anchor", "end")
					    .text("Número de Consultas");

					svg.append("path")
					    .datum(data)
					    .attr("class", "line")
					    .attr("d", line);

					function type(d) {
					  d.date = formatDate.parse(d.date);
					  d.close = +d.close;
					  return d;
					}

				},
				error: function(x,a,s){
					alert("Se ha producido un error...");
					$("#consulta3-ejecutar").prop("disabled", false);
					$("#consulta3-input").prop("disabled",false);
				}
			});
		}else{
			alert("Ingrese un parámetro de búsqueda");
		}
	});
});