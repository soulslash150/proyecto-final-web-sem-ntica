$(document).ready(function(){
	$("#menu-estaticas").addClass("active");

	var diameter= 620;

	$("#boton1").click(function(){
		$("#boton1").prop("disabled", true);
		$("#body-consulta-recurrente").text("Consultando...");
		var query="PREFIX lsqv: <http://bio2rdf.org/logs_vocabulary:> PREFIX bio2rdf: <http://bio2rdf.org/> PREFIX purl: <http://purl.org/dc/terms/> SELECT ?B (COUNT(?B) AS ?TOTAL) WHERE { ?A lsqv:usesFeature ?B}GROUP BY ?B ORDER BY DESC (?TOTAL)";
		$.ajax({
			type: "POST",
			url: "http://localhost:3030/db/query",
			data: {query: query},
			success: function(data){
				$("#body-consulta-recurrente").text("");
				$("#boton1").prop("disabled", false);
				format = d3.format(",d"),
				color = d3.scale.category20c();
				var bubble = d3.layout.pack()
				    .sort(null)
				    .size([diameter, diameter])
				    .padding(1.5);
				var svg = d3.select("#body-consulta-recurrente").append("svg")
				    .attr("width", diameter)
				    .attr("height", diameter)
				    .attr("class", "bubble");
				var root = {
					name: "Grafico",
					children: []
				};
				var resultados = data.results.bindings;
				for(var i = 0; i < resultados.length; i++){
					var aux = resultados[i];
					var obj = {
						name: i,
						children: [{
							name: aux.B.value.split("s:")[1],
							size: aux.TOTAL.value
						}
						]
					};
					root.children.push(obj);
				}
				var node = svg.selectAll(".node")
				    .data(bubble.nodes(classes(root))
				    .filter(function(d) { return !d.children; }))
				  .enter().append("g")
				    .attr("class", "node")
				    .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
				node.append("title")
				    .text(function(d) { return d.className + ": " + format(d.value); });
				node.append("circle")
				    .attr("r", function(d) { return d.r; })
				    .style("fill", function(d) { return color(d.packageName); });
				node.append("text")
				    .attr("dy", ".3em")
				    .style("text-anchor", "middle")
				    .text(function(d) { return d.className.substring(0, d.r / 3); });
				// Returns a flattened hierarchy containing all leaf nodes under the root.
				function classes(root) {
				  var classes = [];
				  function recurse(name, node) {
				    if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
				    else classes.push({packageName: name, className: node.name, value: node.size});
				  }
				  recurse(null, root);
				  return {children: classes};
				}
				d3.select(self.frameElement).style("height", diameter + "px");
			},
			error: function(a,b,c){
				$("#body-consulta-recurrente").text("Se ha producido un error...");
				$("#boton1").prop("disabled", false);
			}
		});
	});

	$("#boton2").click(function(){
		$("#boton2").prop("disabled", true);
		$("#body-droga-buscada").text("Consultando...");
		var query="PREFIX lsqv: <http://bio2rdf.org/logs_vocabulary:> PREFIX bio2rdf: <http://bio2rdf.org/> PREFIX purl: <http://purl.org/dc/terms/> SELECT ?B (COUNT(?B) AS ?TOTAL) WHERE { ?A lsqv:hasUri ?B FILTER regex(str(?B),'[:]DB[0-9]*$') } GROUP BY ?B ORDER BY DESC (?TOTAL)LIMIT 20";
		$.ajax({
			type: "POST",
			url: "http://localhost:3030/db/query",
			data: {query: query},
			success: function(data){
				$("#body-droga-buscada").text("");
				$("#boton2").prop("disabled", false);
				format = d3.format(",d"),
				color = d3.scale.category20c();
				var bubble = d3.layout.pack()
				    .sort(null)
				    .size([diameter, diameter])
				    .padding(1.5);
				var svg = d3.select("#body-droga-buscada").append("svg")
				    .attr("width", diameter)
				    .attr("height", diameter)
				    .attr("class", "bubble");
				var root = {
					name: "Grafico",
					children: []
				};
				var resultados = data.results.bindings;
				for(var i = 0; i < resultados.length; i++){
					var aux = resultados[i];
					var obj = {
						name: i,
						children: [{
							name: aux.B.value.split("k:")[1],
							size: aux.TOTAL.value
						}
						]
					};
					root.children.push(obj);
				}
				var node = svg.selectAll(".node")
				    .data(bubble.nodes(classes(root))
				    .filter(function(d) { return !d.children; }))
				  .enter().append("g")
				    .attr("class", "node")
				    .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
				node.append("title")
				    .text(function(d) { return d.className + ": " + format(d.value); });
				node.append("circle")
				    .attr("r", function(d) { return d.r; })
				    .style("fill", function(d) { return color(d.packageName); });

				node.append("text")
				    .attr("dy", ".3em")
				    .attr("id",function(d){return d.className})
				    .style("text-anchor", "middle")
				    .text(function(d) { return d.className.substring(0, d.r / 3); });
				// Returns a flattened hierarchy containing all leaf nodes under the root.
				function classes(root) {
				  var classes = [];
				  function recurse(name, node) {
				    if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
				    else classes.push({packageName: name, className: node.name, value: node.size});
				  }
				  recurse(null, root);
				  return {children: classes};
				}
				d3.select(self.frameElement).style("height", diameter + "px");

				var resultados = data.results.bindings;
				for(var i = 0; i < resultados.length; i++){
					var nombre = resultados[i].B.value.split("k:")[1]; 
					$("#"+nombre).click(function(){
						var yo = $(this).attr("id");
						$.ajax({
							type: "POST",
							url: "http://bio2rdf.org/sparql?format=application%2Fsparql-results%2Bjson",
							data: {query: "PREFIX purl: <http://purl.org/dc/terms/> PREFIX yay: <http://bio2rdf.org/drugbank:> SELECT * WHERE{ yay:"+yo+" purl:title ?nombre . yay:"+yo+" purl:description ?descripcion }"},
							success: function(data){
								$("#"+yo).text(data.results.bindings[0].nombre.value);
								$("#body-descripcion-droga").text(data.results.bindings[0].descripcion.value);
							},
							error: function(a,b,c){
								console.log("Error");
							}
						});
					});
				}

			},
			error: function(a,b,c){
				$("#body-droga-buscada").text("Se ha producido un error...");
				$("#boton2").prop("disabled", false);
			}
		});
	});
});