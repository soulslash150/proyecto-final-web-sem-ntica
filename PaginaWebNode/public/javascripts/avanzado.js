$(document).ready(function(){
	$("#menu-avanzado").addClass("active");

	$("#boton-ejecutar").click(function(){
		$("#boton-ejecutar").prop("disabled", true);
		$("#textarea").prop("disabled", true);
		$("#box-resultado").text("Ejecutando...Por favor espere");

		$.ajax({
			type: "POST",
			url: "http://localhost:3030/db/query",
			data: {query: $("#textarea").val()},
			success: function(data){
				$("#box-resultado").text(JSON.stringify(data,undefined,2));
				$("#boton-ejecutar").prop("disabled", false);
				$("#textarea").prop("disabled",false);
			},
			error: function(a,b,c){
				$("#box-resultado").text("Se ha producido un error...");
				$("#boton-ejecutar").prop("disabled", false);
				$("#textarea").prop("disabled",false);	
			}
		});
	});
});